___
`yarn add emoji-mart` 安装表情插件
`yarn add prop-types` 安装类型校验 import PropTypes from 'prop-types
`yarn add pubsub-js`  安装发布订阅 import Pubsub from 'pubsub-js'
___


#### 模块
- 向外提供特定功能的js程序, 一般就是一个 js 文件
- 为什么? 随着业务逻辑增加, 代码越来越多且复杂
- 作用: 复用js, 简化 js的编写, 提高 js运行效率
#### 组件
- 用来实现局部功能效果的代码和资源的集合(html/css/js/image 等等)
- 为什么? 一个页面的功能更复杂
- 作用: 复用编码, 简化项目编码, 提高运行效率

#### 什么是 React?
- 用于动态构建用户界面的 Javascript 库(只关注于视图)

#### 为什么使用 React?


#### React的特点
- 1.声明式编码
- 2.组件化编码
- 3.React Native 编写原生应用
- 4.高效(优秀的Diffing算法)
- - 高效原因: 使用虚拟DOM,不总是直接操作界面真实DOM, Diff算法,最小化页面重绘.

```bash
- 受控组件(推荐)/非受控组件
- 简单组件(无state)/复杂组件(有state)
- refs 三种写法: 字符串(不推荐,以后可能会移除) / 回调函数(内敛, 组件数据变化时会触发两次,第一次节点null是销毁,第二次是新建) / 回调函数(定义在当前组件的实例上)
```


#### redux 是一个专门做状态管理的 js 库(集中式管理多个组件状态共享)

#### 纯函数和高阶函数
```
- 1.纯函数, redux的reducer函数必须是一个纯函数
> 一类特别的函数: 只要是同样的输入(实参), 必定得到同样的输出(返回)
> 必须遵守以下一些约束:
>> 1) 不得改写参数数据;
>> 2) 不会产生任何副作用, 例如网络请求, 输入和输出设备
>> 3) 不能调用Date.now()或者Math.random()等不纯的方法
---------------------
- 1.高阶函数
> 一类特别的函数: 参数是函数 || 返回是函数
> 常见的高阶函数:
>> 1) 定时器设置函数
>> 2) 数组的 forEach/map/filter/reduce/find/bind
>> 3) promise
```