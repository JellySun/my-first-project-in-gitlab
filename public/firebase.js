/* eslint-disable */
var token, firebaseConfig = {
  apiKey: "AIzaSyDd8CqES89L6Q_7ghkrmDh0VoHe0g0yU9k",
  authDomain: "fcm-web-testing-d59f1.firebaseapp.com",
  databaseURL: "https://fcm-web-testing-d59f1.firebaseio.com",
  projectId: "fcm-web-testing-d59f1",
  storageBucket: "",
  messagingSenderId: "158153948884",
  appId: "1:158153948884:web:88bf68509883bb21"
};
firebase.initializeApp(firebaseConfig);
// 1. 检索消息对象
const messaging = firebase.messaging();
getToken(messaging);
function getToken () {
  // 2. 在应用中配置网络凭据-->(项目设置->云消息传递->网络配置中的密钥对)
  messaging.usePublicVapidKey("BEJ-kzZViTPAvI33iYxFnu_7Dg1s0Rkh63NoDusmCx646sz4qF24Zm7ma1f8PV8MP1HdC_sM9yaQgzxVlb2Zyrg");
  // 3. 申请接收通知的权限, requestPermission() 方法会显示一个征询用户同意的对话框，让用户向您的应用授予权限，以便在浏览器中接收通知。
  // 如果用户拒绝授予权限，FCM 注册令牌请求会出现错误。
  messaging.requestPermission().then(function(){
    // 4. 获取注册令牌, 注册令牌可能会在发生下列情况时更改:
    // Web 应用删除注册令牌时。
    // 用户清除浏览器数据时。在这种情况下，调用 getToken 可检索新令牌。
    return messaging.getToken();
  }).then(function(currentToken){
    //I get the token here. I use this token to push the notification
    if (currentToken) {
      token = currentToken;
      sessionStorage.setItem("currentToken", JSON.stringify(currentToken))
      console.log("1. getToken: " + currentToken);
    } else {
      console.log('1. No Instance ID token available. Request permission to generate one.');
    }
    setTokenSentToServer();
  }).catch(function(err){
    console.log('1. Unable to get permission to notify. 没有接收通知的权限');
    setTokenSentToServer();
  })
}
function setTokenSentToServer () {
  if (sessionStorage.currentToken) {
    console.log("2. Token has been sent , setTokenSentToServer")
    sendMessage();
  } else {
    getToken();
  }
}
// 5. 消息传递服务需要 firebase-messaging-sw.js 文件。除非您已经有 firebase-messaging-sw.js 文件，否则请创建一个使用该名称的空文件，并在检索令牌之前将该文件放在您网域的根目录中。
/* 6. 发送消息 */
function sendMessage () {
  let data = {
    "to" : JSON.parse(sessionStorage.getItem("currentToken")),
    "collapse_key": "Updates Available",
    "notification": { // 通知
      "title": "FCM From SzjSmiling",
      "body": "❀❀❀❀❀❀   2019年10月1日,是中华人民共和国成立70周年!   👏👏👏👏👏",
      "icon": "/static/media/logo.5d5d9eef.svg",
      "click_action": "https://blog.csdn.net/szjSmiling",
      "image": "https://img.happyeasygo.com/img/1565164155895.jpg"
    },
    "data":{ // 数据
      "Nick" : "Mario",
      "body" : "great match!",
      "Room" : "PortugalVSDenmark"
    }
  }
  fetch('https://fcm.googleapis.com/fcm/send', {
    method: 'POST',
    body: JSON.stringify(data),
    headers: {
    "Content-Type": "application/json",
    "Authorization": "key=AAAAJNK1vtQ:APA91bEsu484VHN6OJTMfDMgpzwJCYs34D0yvipwoxQscmWpVltSBNo0tW-jJt52dLlrzOoUHtcWOfdBhWTAU9oggCCyBkfVLfVdT8-NUabiIcFKfn-o8CduKBehGdd7_bvathK73JLb"
  }}).then(res => {
    console.log("3. messaging send Successfully "+ res.status + ', 请求地址: ' + res.url)
    if (res.status === 200){
      console.log("4. messaging done: ", messaging)
    }
  }).catch(error => {
    console.log(error)
  })

  messaging.onMessage(function(payload){
    console.log('5. messaging received: ', payload)
  })
}

// HttpRequest(paramObj, function (response) {
//   console.log(response)
// }, function () {
//   confirm("网络错误!")
// })
function HttpRequest (paramObj, fun, errfun) {
  let xmlHttp = null;
  /*
    *创建XMLHttpRequest对象，
    *老版本的 Internet Explorer（IE5 和 IE6）使用 ActiveX 对象：new ActiveXObject("Microsoft.XMLHTTP")
  */
  if (window.XMLHttpRequest) {
    xmlHttp = new XMLHttpRequest();
  } else if (window.ActiveXObject) {
    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  // 判断是否支持请求
  if (xmlHttp == null) {
    alert('您的浏览器不支持XMLHttp')
    return false
  }
  let httpType = (paramObj.type || 'GET').toUpperCase()
  let dataType = paramObj.dataType || 'json'
  let httpUrl = paramObj.httpUrl || ''
  let async = paramObj.async || 'true'
  // 请求参数--post 请求参数格式: foo=bar&lerem=ipsum
  let paramData = paramObj.data || []
  // let requestData = ''
  // for(let name in paramData) {
  //   requestData += name +'=' +paramData[name] + '&'
  // }
  // requestData = requestData == '' ? '' : requestData.substring(0, requestData.length - 1)
  console.log(paramData)
  /* 接口连接, 先判断连接类型是 post 还是 get */
  if(httpType === 'GET') {
    xmlHttp.open("GET", httpUrl, async);
  }else if(httpUrl === 'POST') {
    xmlHttp.open("POST", httpUrl, async);
  }
  // 发送合适的请求头信息
  xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
  xmlHttp.setRequestHeader("Authorization", "key=AAAAJNK1vtQ:APA91bEsu484VHN6OJTMfDMgpzwJCYs34D0yvipwoxQscmWpVltSBNo0tW-jJt52dLlrzOoUHtcWOfdBhWTAU9oggCCyBkfVLfVdT8-NUabiIcFKfn-o8CduKBehGdd7_bvathK73JLb")
  xmlHttp.send(paramData)
  /* 请求接收 */
  xmlHttp.onreadystatechange = function () {
    console.log(xmlHttp.readyState)
    if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
      /* 成功的回调 */
      fun(xmlHttp.responseText)
    } else {
      /* 失败的回调 */
      errFun()
    }
  }
}