import React from 'react';
import logo from '@/logo.svg';
import './App.css';
import NavBar from '@/components/NavBar/NavBar.js';
class App extends React.Component {
  constructor (props) {
    super(props);
    this.router = props;
    this.startLearning = this.startLearning.bind(this);
  }
  startLearning (e) {
    this.router.history.push('/home')
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>click&nbsp;&nbsp;
            <code className="App-sign" onClick={this.startLearning}>this</code>
            &nbsp;&nbsp;and start to learning React.</p>
          <a className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
            >Learn React</a>
          <NavBar />
        </header>
      </div>
    )
  }
}

export default App;
