import React from 'react';
import './HomeHead.css';
import HeadTheme from './component/HeadTheme.js';

// 定义组件的方法一: 类ES6定义组件
export default class HomeHead extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      searchFocus: false,
      showHeadTheme: false,
    }
  }
  componentDidMount() {
    document.addEventListener('click', this.hideAllMenu);
  }

  searchOnBlur = () => {
    this.setState(() => ({
      searchFocus: false
    }))
  }
  searchOnFocus = () => {
    this.setState(() => ({
      searchFocus: true
    }))
  }
  switchThemeEvent = (event) => {
    event.nativeEvent.stopImmediatePropagation();
    this.setState((val) => {
      return { showHeadTheme: !val.showHeadTheme }
    })
  }
  hideAllMenu = (event) => {
    if (event.target.name !== 'switchThemeEvent') {
      this.setState({
        showHeadTheme: false,
      })
    }
  }
  render () {
    return (
      <header style={{width: '100%'}}>
        <div className="h-header">
          <div className="hh-cont clear">
            <div className="hhc-logo height56">
              <img src={require('@/assets/images/logo_jianshu.jpg')} alt="简书" title="简书" />
            </div>
            <div className="hhc-right height56 flex justify-start relative">
              <HeadTheme showHeadTheme={this.state.showHeadTheme} />
              <i className="hhcr-theme pointer" name="switchThemeEvent" onClick={this.switchThemeEvent}>Aa</i>
              <img className="hhcr-zuan pointer" alt="简书钻" title="简书钻"
                src={require('@/assets/images/logo_zuanzuan.jpg')} />
              <a className="hhcr-login flex" href="">
                <span>登录</span>
              </a>
              <a className="hhcr-register flex" href="">
                <span>注册</span>
              </a>
              <a className="hhcr-article flex" href="">
                <i className="iconfont icon-edit"></i>
                <span>写文章</span>
              </a>
            </div>
            {/* 浮动元素 和 非浮动元素并列布局时,浮动元素的左边要是临界或者是浮动元素 */}
            <div className="hhc-titles height56 box-content">
              <div className="hhct-cont flex justify-start">
                <nav className="hhct-pages">
                  <a className="height56" href="">首页</a>
                  <a className="height56" href="">个人中心</a>
                </nav>
                <div className="hhct-search">
                  <input type="text" placeholder="搜索"
                  className={`hhcts-input box-border ${this.state.searchFocus?'hhcts-focus':''}`}
                  onBlur={this.searchOnBlur}
                  onFocus={this.searchOnFocus} />
                  <i className={`hhcts-btn iconfont icon-i-search`}></i>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="h-header-relative height56"></div>
      </header>
    )
  }
}