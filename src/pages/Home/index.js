import React from 'react';
import HomeHead from './HomeHead.js';
import HomeFoot from './HomeFoot.js';
import HomeContent from './HomeContent.js';
// es6 创建组件
export default class Home extends React.Component{
  render () {
    return (
      <div>
        <HomeHead />
        <HomeContent />
        <HomeFoot />
      </div>
    )
  }
}