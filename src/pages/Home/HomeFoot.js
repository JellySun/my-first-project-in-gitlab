import React from 'react';
import { emojify } from 'react-emojione';
import './HomeFoot.css';
import ShareDialog from './component/ShareDialog.js';
import IconEmoji from '@/components/IconEmoji/IconEmoji.js';
const options = {
  convertShortnames: true,
  convertUnicode: true,
  convertAscii: true,
  style: {
    height: 16,
    margin: 1,
    cursor: 'pointer'
  }
}
class HomeFoot extends React.Component {
  constructor () {
    super();
    this.state = {
      comments: '',
      showFocusStyle: false,
      canPublish: false,
      showShareDialog: false,
      selectShareText: '',
      showEmojiDialog: false,
      shareList: [
        {id: 0, title: 'aaaa'},
        {id: 1, title: 'bbbb'},
        {id: 2, title: 'cccc'},
      ]
    };
    this.inputRef = React.createRef();
  }
  componentDidMount() {
    document.addEventListener('click', this.hideAllMenu);
  }

  inputOnFocus = () => { // 输入激活时的样式控制
    this.setState({
      showFocusStyle: true
    })
  }
  inputChange = (e) => { // 输入内容时可以发布,否则不可
    let text = e.target.value;
    this.setState({
      canPublish: text.length > 0? true : false,
      comments: text
    })
  }
  cancelComment = () => {
    this.setState({
      comments: '',
      canPublish: false,
      showEmojiDialog: false,
      showFocusStyle: false
    })
  }
  publishComment = () => {
    this.setState({
      showEmojiDialog: false,
      showFocusStyle: false
    })
  }
  controlShareDialogEvent = (event) => {
    event.nativeEvent.stopImmediatePropagation();
    // e.stopPropagation(); // 阻止合成事件的事件冒泡
    this.setState((val) => {
      return {
        showShareDialog: !val.showShareDialog
      }
    })
  }
  changeShareItem = (val) => {
    this.setState(() => {
      return {
        selectShareText: val.title,
        showShareDialog: false,
      }
    }, () => { // setState是异步操作，但是我们可以在它的回调函数里面进行操作
      console.log(`选择的子项是: ${this.state.selectShareText}`)
    })
  }
  inputShowSelectedIcon = (icon) => {
    this.setState((val) => {
      return {
        showEmojiDialog: false,
        comments: `${val.comments}${icon}`
      }
    }, () => {
      this.inputRef.current.focus();
      console.log(this.state.comments)
    })
  }
  showEmojiEvent = (event) => {
    event.nativeEvent.stopImmediatePropagation();
    this.setState((val) => {
      return { showEmojiDialog: !val.showEmojiDialog }
    }, () => {
      this.inputRef.current.focus();
    })
  }
  hideAllMenu = (event) => {
    if (event.target.name !== 'showEmojiEvent' ||
      event.target.name !== 'controlShareDialogEvent'
    ) {
      this.setState({
        showEmojiDialog: false,
        showShareDialog: false,
      })
    }
  }
  render () {
    return (
      <footer className="home-footer ">
        <div className={`home-footer-box flex justify-start ${this.state.showFocusStyle?'align-end':''}`}>
          <div className="h-footer-left">
            <textarea type="text"
            className={`box-border ${this.state.showFocusStyle?'input-active':''}`}
            placeholder="写下你的评论..."
            ref={this.inputRef}
            value={this.state.comments}
            onFocus={this.inputOnFocus}
            onChange={this.inputChange}>
            </textarea>
            <p className={`real-input box-border ${this.state.showFocusStyle?'input-active':''}`}>
              <span className="like-placeholder"
              style={{display: (this.state.showFocusStyle || this.state.comments !== '')?'none':'block'}}>写下你的评论...</span>
              {emojify(this.state.comments, options)}
            </p>
            <i className="iconfont icon-xiaolian" name="showEmojiEvent"
            style={{display: this.state.showFocusStyle?'block':'none'}}
            onClick={this.showEmojiEvent}></i>
            <IconEmoji showEmojiDialog={this.state.showEmojiDialog} selectEmojiP={this.inputShowSelectedIcon}/>
          </div>
          <div style={{display: this.state.showFocusStyle?'none':'block'}}>
            <div className="h-footer-right flex justify-start">
              <div className="hfr-comment flex">
                <i className="iconfont icon-pinglun2" />
                <p>评论(0)</p>
              </div>
              <div className="hfr-praise flex">
                <i className="iconfont icon-dianzan" />
                <p>赞</p>
              </div>
              <div className="hfr-more">
                <i className="iconfont icon-shenglvehao" name="controlShareDialogEvent"
                onClick={this.controlShareDialogEvent}></i>
                <ShareDialog showShareDialog={this.state.showShareDialog}
                  shareList={this.state.shareList}
                  changeShareItemP={this.changeShareItem} />
              </div>
            </div>
          </div>
          <div className="hf-focus" style={{display: this.state.showFocusStyle?'block':'none'}}>
            <button className={`focus-publish`}
            style={{cursor:this.state.canPublish?'pointer':'not-allowed', opacity: this.state.canPublish?'1':'0.5'}}
            disabled={this.state.canPublish?'':'disabled'}
            onClick={this.publishComment}>发布</button>
            <button className="focus-cancel" onClick={this.cancelComment}>取消</button>
          </div>
        </div>
      </footer>
    )
  }
}
export default HomeFoot;