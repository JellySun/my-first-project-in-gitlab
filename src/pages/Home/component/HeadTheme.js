import React from 'react';
import './HeadTheme.css';
import Button from 'antd/lib/button'; // 加载js
import 'antd/lib/button/style/css'; // 加载css
// import 'antd/lib/button/style'; // 加载less
export default class HeadTheme extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      showNightPattern: false,
      showFBlackPattern: false,
      showFSimplifyPattern: true,
    }
  }
  switchNightPattern  = (data) => {
    this.setState(() => ({
      showNightPattern: data
    }), () => {
      document.body.style.backgroundColor = data ? '#2d2d2d' : '#f9f9f9';
    })
  }
  switchFBlackPattern  = (data) => {
    this.setState(() => ({
      showFBlackPattern: data
    }), () => {
      document.body.style.fontFamily = data ? 'Songti SC' : 'Microsoft YaHei';
    })
  }
  switchFSimplifyPattern  = (data) => {
    this.setState(() => ({
      showFSimplifyPattern: data
    }), () => {
      // document.body.style.fontFamily = data ? '#2d2d2d' : '#f9f9f9';
    })
  }
  render () {
    return (
      <div className={`head-theme borderHornB absolute box-content ${this.props.showHeadTheme?'soUpBlock':'soUpNone'}`}>
        <div className="ht-nightOrDay flex justify-between">
          <p className="htn-title"><i className="iconfont icon-yewan"></i>夜间模式</p>
          <div className="htn-btns pointer">
            <button className={`box-border htnb-open ${this.state.showNightPattern?'htnb-active':''}`}
            onClick={this.switchNightPattern.bind(this, true)}>开</button>
            <button className={`box-border htnb-close ${!this.state.showNightPattern?'htnb-active':''}`}
            onClick={this.switchNightPattern.bind(this, false)}>关</button>
          </div>
        </div>
        <div className="ht-line"></div>
        <div className="ht-font-family htn-btns pointer">
          <button className={`box-border htnb-open ${this.state.showFBlackPattern?'htnb-active':''}`}
          onClick={this.switchFBlackPattern.bind(this, true)}>宋体</button>
          <button className={`box-border htnb-close ${!this.state.showFBlackPattern?'htnb-active':''}`}
          onClick={this.switchFBlackPattern.bind(this, false)}>黑体</button>
        </div>
        <div className="ht-font-style htn-btns pointer">
          <button className={`box-border htnb-open ${this.state.showFSimplifyPattern?'htnb-active':''}`}
          onClick={this.switchFSimplifyPattern.bind(this, true)}>简体</button>
          <button className={`box-border htnb-close ${!this.state.showFSimplifyPattern?'htnb-active':''}`}
          onClick={this.switchFSimplifyPattern.bind(this, false)}>繁体</button>
        </div>
      </div>
    )
  }
}